## Squad
-----
The squad source code is available at https://github.com/Linaro/squad

### API 
The api for squad is available at : https://qa-reports.linaro.org/api/ where it includes multiple api subdirectories : 

![image squad_api](api.png)

#### Squad Uses in the pipeline 
---- 
##### 1- submit tuxsuite 
It is one of the commands of the cmdline tool `squad-client`. 
After finishing the build job, `build.json`, artifacts, and build artifacts are generated. these results are submitted to squad and fitted in the squad schema. this is done using the squad `submit_tuxsuite` command of the `squad-client`. this is done in the already downloaded `build-afterscript.sh` that was downloaded as part of the prerequisites. 
```
squad-client --squad-token="${QA_REPORTS_TOKEN}"  --squad-host="${QA_SERVER}" 
submit-tuxsuite --group="${QA_TEAM}" --project="${QA_PROJECT}"
--build="${git_desc}" --backend tuxsuite.com --json build.json
```
This is shown in the block diagram above.

The implementation of the `submit-tuxsuite` command within the squad-client is available in
https://github.com/Linaro/squad-client/blob/d288f92e94fbde8ade5bdf4215374e66ae0f803d/squad_client/commands/submit_tuxsuite.py

##### 2- squad report


```
squad-report 
    --url="${QA_SERVER}" 
    --group="$QA_TEAM" 
    --project="$QA_PROJECT" 
    --build="${BUILD_ID}" 
    --email-subject="${QA_TEAM} ${REPORT_TYPE} for ${BUILD_ID}${suffix}" 
    --config-report-type="${REPORT_TYPE}" 
    "${SQUAD_REPORT_EXTRA_OPTIONS}"
 ```
as shown in the block diagram above.


##### 3- Create or update project 
This is run as part of the check-project pipeline, which runs the `create-or-update-project` command that is part of `squad-client` as shown below: 

```
squad-client --squad-host "${QA_SERVER}" --squad-token "${QA_REPORTS_TOKEN}" 
create-or-update-project 
    --group "${QA_TEAM}" --slug "${slug}" 
    --is-public --name "${name}" 
    --plugins linux_log_parser,ltp 
    --wait-before-notification-timeout "${QA_WAIT_BEFORE_NOTIFICATION_TIMEOUT}" 
    --notification-timeout "${QA_NOTIFICATION_TIMEOUT}" 
    --force-finishing-builds-on-timeout "${QA_FORCE_FINISHING_BUILDS_ON_TIMEOUT}" 
    --important-metadata-keys "build-url,git_ref,git_describe,git_repo,kernel_version" 
    --thresholds "build/*-warnings"
    
 ```

in the above command, if `REGISTER_CALLBACK_TOKEN` is defined, then an extra two options to the above command is added: 
```
 --data-retention 0 
 --settings "CALLBACK_HEADERS: {PRIVATE-TOKEN: $REGISTER_CALLBACK_TOKEN}"
```

as shown in the block diagram above.


The above command code is located at https://github.com/Linaro/squad-client/blob/d288f92e94fbde8ade5bdf4215374e66ae0f803d/squad_client/commands/create_or_update_project.py
