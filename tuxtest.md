## TuxTest


Implements the testing phase of the tuxsuite as part of the pipeline. 
requires the linux kernel image and the artifacts produced from the linux build process, in addition to the various parameters specifing the test scripts and the target platform. 

![image info](tuxtest.png)

